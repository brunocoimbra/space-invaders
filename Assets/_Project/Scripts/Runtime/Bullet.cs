﻿using UnityEngine;

namespace SpaceInvaders
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rigidbody = default;
        [SerializeField] private Trigger2DListener _trigger2DListener = default;
        [SerializeField] private BulletView _view = default;
        [SerializeField] private BulletData _data = default;

        private EntityType _entity;

        public void Initialize(EntityType entity, Vector2 direction)
        {
            _entity = entity;

            _trigger2DListener.OnTriggerEnter += HandleTriggerEnter;

            transform.up = direction;
            Vector2 force = transform.up * _data.Force;
            _rigidbody.AddForce(force, ForceMode2D.Impulse);
        }

        private void HandleTriggerEnter(Collider2D other)
        {
            var handler = other.GetComponent<IBulletCollisionHandler>();

            if (handler != null)
            {
                handler.ApplyBulletDamage(_entity);

                if (handler.ValidateBulletCollision(_entity))
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
