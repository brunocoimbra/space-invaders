using System;
using UnityEngine;

namespace SpaceInvaders
{
    public class DamageListener : MonoBehaviour, IBulletCollisionHandler
    {
        public event Action OnDamageTaken;

        [SerializeField] private EntityType _entityType = default;

        void IBulletCollisionHandler.ApplyBulletDamage(EntityType sender)
        {
            if (sender != _entityType)
            {
                OnDamageTaken?.Invoke();
            }
        }

        bool IBulletCollisionHandler.ValidateBulletCollision(EntityType sender)
        {
            return sender != _entityType;
        }
    }
}
