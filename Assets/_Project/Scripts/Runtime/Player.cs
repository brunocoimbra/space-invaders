using UnityEngine;

namespace SpaceInvaders
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private PlayerView _view = default;
        [SerializeField] private DamageListener _damageListener = default;
        [Space]
        [SerializeField] private PlayerData _data = default;

        public void Initialize()
        {
            _damageListener.OnDamageTaken += HandleDamageTaken;
        }

        public void Move(float direction)
        {
            float x = direction * _data.MovementSpeed * Time.deltaTime;
            Vector3 movement = x * Vector3.right;
            transform.Translate(movement, Space.Self);
        }

        public void Shoot()
        {
            Bullet instance = Instantiate(_data.BulletPrefab, transform.position, transform.rotation);
            instance.Initialize(EntityType.Player, Vector2.up);
        }

        private void HandleDamageTaken()
        {
            Debug.Log("PlayerDamageTaken");
        }
    }
}
