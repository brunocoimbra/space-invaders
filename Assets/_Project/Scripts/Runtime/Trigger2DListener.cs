﻿using UnityEngine;

namespace SpaceInvaders
{
    public class Trigger2DListener : MonoBehaviour
    {
        public delegate void TriggerHandler(Collider2D other);

        public event TriggerHandler OnTriggerEnter;
        public event TriggerHandler OnTriggerExit;
        public event TriggerHandler OnTriggerStay;

        private void OnTriggerEnter2D(Collider2D other)
        {
            OnTriggerEnter?.Invoke(other);
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            OnTriggerStay?.Invoke(other);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            OnTriggerExit?.Invoke(other);
        }
    }
}
