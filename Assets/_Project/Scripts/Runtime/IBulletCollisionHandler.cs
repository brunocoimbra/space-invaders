﻿namespace SpaceInvaders
{
    public interface IBulletCollisionHandler
    {
        void ApplyBulletDamage(EntityType sender);

        bool ValidateBulletCollision(EntityType sender);
    }
}
