using UnityEngine;

namespace SpaceInvaders
{
    public class GameSystem : MonoBehaviour
    {
        [SerializeField] private PlayerManager _playerManager = default;
        [SerializeField] private InputManager _inputManager = default;
        [Space]
        [SerializeField] private GameData _gameData = default;

        private void Awake()
        {
            _playerManager.Initialize(_gameData);
            _inputManager.Initialize();
        }

        private void OnEnable()
        {
            _inputManager.Enable();
        }

        private void OnDisable()
        {
            _inputManager.Disable();
        }

        private void Start()
        {
            _inputManager.OnMove += _playerManager.OnMove;
            _inputManager.OnShoot += _playerManager.OnShoot;
        }

        private void Update()
        {
            _playerManager.OnUpdate();
        }
    }
}
