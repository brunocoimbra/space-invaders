﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace SpaceInvaders
{
    public class InputManagerData : ScriptableObject
    {
        [SerializeField] private InputAction m_Move = default;
        [SerializeField] private InputAction m_Shoot = default;
        [SerializeField] private InputAction m_Pause = default;

        public InputAction Move => m_Move;
        public InputAction Pause => m_Pause;
        public InputAction Shoot => m_Shoot;
    }
}
