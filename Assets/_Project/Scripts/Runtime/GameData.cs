﻿using UnityEngine;

namespace SpaceInvaders
{
    public class GameData : ScriptableObject
    {
        [SerializeField] private float _playableAreaHalf = default;

        public float PlayableAreaHalf => _playableAreaHalf;
    }
}
