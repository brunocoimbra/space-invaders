using UnityEngine;

namespace SpaceInvaders
{
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private float m_MovementSpeed = default;
        [Space]
        [SerializeField] private Bullet m_BulletPrefab = default;

        public float MovementSpeed => m_MovementSpeed;
        public Bullet BulletPrefab => m_BulletPrefab;
    }
}
