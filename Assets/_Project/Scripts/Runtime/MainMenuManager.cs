using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SpaceInvaders
{
    public class MainMenuManager : MonoBehaviour
    {
        public event UnityAction OnExitButtonClick
        {
            add => _exitButton.onClick.AddListener(value);
            remove => _exitButton.onClick.RemoveListener(value);
        }
        public event UnityAction OnPlayButtonClick
        {
            add => _playButton.onClick.AddListener(value);
            remove => _playButton.onClick.RemoveListener(value);
        }
        public event UnityAction OnScoreboardButtonClick
        {
            add => _scoreboardButton.onClick.AddListener(value);
            remove => _scoreboardButton.onClick.RemoveListener(value);
        }

        [SerializeField] private Button _playButton = default;
        [SerializeField] private Button _scoreboardButton = default;
        [SerializeField] private Button _exitButton = default;
    }
}
