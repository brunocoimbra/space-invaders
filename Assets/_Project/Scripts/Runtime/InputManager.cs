﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace SpaceInvaders
{
    public class InputManager : MonoBehaviour
    {
        public delegate void MoveHandler(float movement);

        public event MoveHandler OnMove;
        public event Action OnPause;
        public event Action OnShoot;

        [SerializeField] private InputManagerData _data = default;

        public void Initialize()
        {
            _data.Move.performed += HandleMovePerformed;
            _data.Shoot.performed += HandleShootPerformed;
            _data.Pause.performed += HandlePausePerformed;
        }

        public void Enable()
        {
            _data.Move.Enable();
            _data.Shoot.Enable();
            _data.Pause.Enable();
        }

        public void Disable()
        {
            _data.Move.Disable();
            _data.Shoot.Disable();
            _data.Pause.Disable();
        }

        private void HandleMovePerformed(InputAction.CallbackContext context)
        {
            var movement = context.ReadValue<float>();
            OnMove?.Invoke(movement);
        }

        private void HandlePausePerformed(InputAction.CallbackContext context)
        {
            OnPause?.Invoke();
        }

        private void HandleShootPerformed(InputAction.CallbackContext context)
        {
            OnShoot?.Invoke();
        }
    }
}
