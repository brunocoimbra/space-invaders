﻿using UnityEngine;

namespace SpaceInvaders
{
    public class PlayerManager : MonoBehaviour
    {
        [SerializeField] private Player _player = default;

        private float _currentMovementDirection;
        private GameData _gameData;

        public void Initialize(GameData gameData)
        {
            _gameData = gameData;

            _player.Initialize();
        }

        public void OnUpdate()
        {
            _player.Move(_currentMovementDirection);

            Vector3 position = _player.transform.position;
            position.x = Mathf.Clamp(position.x, -1 * _gameData.PlayableAreaHalf, _gameData.PlayableAreaHalf);
            _player.transform.position = position;
        }

        public void OnMove(float direction)
        {
            _currentMovementDirection = direction;
        }

        public void OnShoot()
        {
            _player.Shoot();
        }
    }
}
