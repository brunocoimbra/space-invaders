using PainfulSmile;
using UnityEditor;
using UnityEngine;

namespace SpaceInvaders
{
    public class SceneManager : MonoBehaviour
    {
        [SerializeField, SceneSelector] private string _gameScene = default;

        public void CloseGame()
        {
#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
        }

        public void OpenGameScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(_gameScene);
        }
    }
}
