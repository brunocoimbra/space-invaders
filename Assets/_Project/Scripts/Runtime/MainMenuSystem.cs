﻿using UnityEngine;

namespace SpaceInvaders
{
    public class MainMenuSystem : MonoBehaviour
    {
        [SerializeField] private MainMenuManager _mainMenuManager = default;
        [SerializeField] private SceneManager _sceneManager = default;

        private void Awake()
        {
            _mainMenuManager.OnExitButtonClick += _sceneManager.CloseGame;
            _mainMenuManager.OnPlayButtonClick += _sceneManager.OpenGameScene;
        }
    }
}
