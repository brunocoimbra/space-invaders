﻿using UnityEngine;

namespace SpaceInvaders
{
    public class BulletData : ScriptableObject
    {
        [SerializeField] private float _force = default;

        public float Force => _force;
    }
}
